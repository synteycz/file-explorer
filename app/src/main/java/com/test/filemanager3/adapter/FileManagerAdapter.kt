package com.test.filemanager3.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.filemanager3.R
import com.test.filemanager3.model.FileItem
import com.test.filemanager3.viewmodel.FileManagerViewModel
import kotlinx.android.synthetic.main.file_item.view.*

class FileManagerAdapter(
        private val clickListener: (FileItem) -> Unit,
        private val longClickListener: (FileItem) -> Boolean
) : RecyclerView.Adapter<FileManagerAdapter.FileManagerViewHolder>() {

    private val items = mutableListOf<FileItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileManagerViewHolder {
        return FileManagerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.file_item,
                parent, false))
    }

    override fun onBindViewHolder(holder: FileManagerViewHolder, position: Int) {
        holder.bind(items[position], clickListener, longClickListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateData(data: List<FileItem>?) {
        items.clear()
        if (data != null) {
            items.addAll(data)
        }
        notifyDataSetChanged()
    }

    class FileManagerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(fileItem: FileItem, clickListener: (FileItem) -> Unit, longClickListener: (FileItem) -> Boolean) {
            itemView.tvName.text = fileItem.name
            itemView.tvDetails.text = fileItem.details

            if (fileItem.type == FileManagerViewModel.FOLDER) {
                itemView.ivIcon.setImageResource(R.drawable.folder)
            } else if (fileItem.type == FileManagerViewModel.FILE) {
                itemView.ivIcon.setImageResource(R.drawable.file)
            }

            if (fileItem.isSelected) {
                itemView.setBackgroundColor(Color.LTGRAY)
            } else {
                itemView.setBackgroundResource(R.color.colorBackground)
            }

            itemView.setOnClickListener { clickListener(fileItem)}
            itemView.setOnLongClickListener { longClickListener(fileItem) }
        }
    }
}