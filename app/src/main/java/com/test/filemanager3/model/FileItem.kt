package com.test.filemanager3.model

data class FileItem(
        var name: String,
        var type: Int,
        var details: String,
        var isSelected: Boolean
)
