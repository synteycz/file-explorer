package com.test.filemanager3.view

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.support.v7.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.test.filemanager3.R
import com.test.filemanager3.adapter.FileManagerAdapter
import com.test.filemanager3.model.FileItem
import com.test.filemanager3.viewmodel.FileManagerViewModel
import kotlinx.android.synthetic.main.activity_file_manager.*


class FileManagerActivity : AppCompatActivity() {

    private lateinit var viewModel: FileManagerViewModel
    private lateinit var mAdapter: FileManagerAdapter

    private val AUTHORITY = "com.test.fileprovider"
    private val REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_manager)
        setSupportActionBar(toolbar)

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            //ask for permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION)
            }
        }

        val settings = PreferenceManager.getDefaultSharedPreferences(this)

        mAdapter = FileManagerAdapter({ fileItem : FileItem -> itemClicked(fileItem) },
                {fileItem: FileItem -> itemLongClicked(fileItem) })

        if (resources.configuration.orientation == ORIENTATION_PORTRAIT) {
            rvFiles.layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.one_column))
        } else if (resources.configuration.orientation == ORIENTATION_LANDSCAPE) {
            rvFiles.layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.more_columns))
        }
        rvFiles.adapter = mAdapter

        viewModel = ViewModelProviders.of(this).get(FileManagerViewModel::class.java)

        if (viewModel.multiSelect) {
            startSupportActionMode(createCAB())
        } else {
            val path = settings.getString(getString(R.string.key_default_folder), Environment.getExternalStorageDirectory().toString())

            viewModel.setPath(path)
        }

        viewModel.files.observe(this, Observer { files ->
            mAdapter.updateData(files)
            if (files != null) {
                if (files.isEmpty()) {
                    emptyFolderLayout.visibility = View.VISIBLE
                } else {
                    emptyFolderLayout.visibility = View.GONE
                }
            }
        })
    }

    private fun itemClicked(fileItem: FileItem) {
        if (viewModel.multiSelect) {
            viewModel.selectItem(fileItem)
        } else {
            if (fileItem.type == FileManagerViewModel.FOLDER) {
                viewModel.changePath(fileItem.name)
            } else {
                val i = Intent(Intent.ACTION_VIEW, FileProvider.getUriForFile(this,
                        AUTHORITY, viewModel.getFile(fileItem.name)))

                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                startActivity(i)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            val grantResultsLength = grantResults.size
            if (grantResultsLength > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                viewModel.refreshDirectory()
            } else {
                Toast.makeText(this, getString(R.string.no_permission_message),
                    Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun itemLongClicked(fileItem: FileItem): Boolean {
        if (viewModel.multiSelect) {
            return false
        }
        startSupportActionMode(createCAB())
        viewModel.selectItem(fileItem)
        return true
    }

    private fun createCAB(): ActionMode.Callback {
        return object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                viewModel.multiSelect = true
                val inflater = mode.menuInflater
                inflater.inflate(R.menu.contextual_menu, menu)

                return true
            }

            override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                deleteFileWithDialog(mode)
                return true
            }

            override fun onDestroyActionMode(mode: ActionMode) {
                viewModel.clearSelection()
                viewModel.multiSelect = false
            }
        }
    }


    private fun deleteFileWithDialog(mode: ActionMode) {
        val dialog = AlertDialog.Builder(this).setTitle(getString(R.string.delete_title))
                .setMessage(getString(R.string.delete_confirmation_message))
                .setPositiveButton(getString(R.string.yes_button), { _, _ ->
                    if (viewModel.deleteSelected()) {
                        Toast.makeText(this, getString(R.string.item_deleted),
                                Toast.LENGTH_LONG).show()
                        mode.finish()
                    } else {
                        Toast.makeText(this, getString(R.string.item_not_deleted),
                                Toast.LENGTH_LONG).show()
                    }
                })
                .setNegativeButton(getString(R.string.cancel_button), { _, _ -> })
        dialog.show()
    }

    override fun onBackPressed() {
        if (!viewModel.goBack()) {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_file_manager, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsPrefActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_refresh -> {
                viewModel.refreshDirectory()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
