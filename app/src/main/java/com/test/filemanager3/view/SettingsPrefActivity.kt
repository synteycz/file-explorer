package com.test.filemanager3.view

import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.Preference
import android.preference.PreferenceFragment
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.test.filemanager3.R
import java.io.File


class SettingsPrefActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_pref)

        fragmentManager.beginTransaction().replace(R.id.main_container, MainPreferenceFragment()).commit()
    }

    class MainPreferenceFragment : PreferenceFragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_main)

            val defaultFolderPref: EditTextPreference =
                    findPreference(getString(R.string.key_default_folder)) as EditTextPreference
            defaultFolderPref.summary = defaultFolderPref.text
            defaultFolderPref.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { preference, newValue ->
                when (!File(newValue.toString()).exists()) {
                    true -> {
                        Toast.makeText(activity, getString(R.string.path_not_exist), Toast.LENGTH_LONG).show()
                        false
                    }
                    false -> {
                        preference.summary = newValue.toString()
                        true
                    }
                }
            }
        }
    }
}
