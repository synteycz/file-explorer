package com.test.filemanager3.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.os.AsyncTask
import android.os.Environment
import com.test.filemanager3.model.FileItem
import java.io.File
import java.util.*

class FileManagerViewModel: ViewModel() {
    var files: MutableLiveData<List<FileItem>> = MutableLiveData()
    private var mActualFolder: File? = null
    private val topLevel: String = Environment.getExternalStorageDirectory().toString()

    var multiSelect: Boolean = false

    companion object {
        const val FOLDER = 0
        const val FILE = 1
    }

    fun setPath(path: String) {
        mActualFolder = File(path)
        refreshDirectory()
    }

    fun changePath(path: String) {
        val newPath: String = mActualFolder?.path + "/" + path
        mActualFolder = File(newPath)
        refreshDirectory()
    }

    fun goBack(): Boolean {
        return if (mActualFolder?.path == topLevel) {
            false
        } else {

            mActualFolder = mActualFolder?.parentFile
            if (mActualFolder == null) false
            else {
                refreshDirectory()
                true
            }
        }
    }

    fun getFile(file: String) : File {
        val url: String = mActualFolder?.path + "/" + file
        return File(url)
    }

    fun refreshDirectory() {
        Thread().run { files.value = loadFiles() }
    }

    fun selectItem(item: FileItem) {
        val list = files.value
        if (list != null) {
            for (aFile in list) {
                if (aFile.name == item.name) {
                    aFile.isSelected = !aFile.isSelected
                    break
                }
            }

            files.value = list
        }
    }

    fun clearSelection() {
        val list = files.value
        if (list != null) {
            for (aFile in list) {
                aFile.isSelected = false
            }

            files.value = list
        }
    }

    fun deleteSelected(): Boolean {
        val list = files.value
        if (list != null) {
            for (aFile in list) {
                if (aFile.isSelected) {
                    if (!File(mActualFolder?.path + "/" + aFile.name).deleteRecursively()) {
                        return false
                    }
                }
            }
            refreshDirectory()
            return true
        }
        return false
    }

    private fun loadFiles(): List<FileItem> {
        val items: ArrayList<FileItem> = ArrayList()
        val files = mActualFolder?.listFiles { file -> !file.isHidden } // get list of all available files in current folder
        if (files != null) {
            for (aFile in files) {
                val item: FileItem
                item = if (aFile.isDirectory) {
                    FileItem(aFile.name, FOLDER, getSummary(aFile), false)
                } else {
                    val size = (aFile.length() / 1024).toDouble()
                    if (size > 1024) {
                        FileItem(aFile.name, FILE, String.format("%.1f MB", size / 1024), false)
                    } else {
                        FileItem(aFile.name, FILE, String.format("%.1f kB", size), false)
                    }
                }
                items.add(item)
            }
        }

        items.sortWith(compareBy( {it.type}, {it.name}))

        return items
    }

    private fun getSummary(file: File?): String {
        val files = file?.listFiles()
        var filesCount = 0
        var foldersCount = 0
        if (files != null) {
            for (aFile in files) {
                if (aFile.isDirectory) {
                    foldersCount++
                } else {
                    filesCount++
                }
            }
        }

        return "Files: $filesCount, Folders: $foldersCount"
    }
}